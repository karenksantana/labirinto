package br.com.ia.model;

import java.util.Iterator;
import java.util.LinkedList;

/**
*
* @author oberdan.pinheiro
*/
public class No {
   
   public static final String MOVER_CIMA = "C";
   public static final String MOVER_BAIXO = "B";
   public static final String MOVER_ESQUERDA = "E";
   public static final String MOVER_DIREITA = "D";
   
   private String estado;
   private No pai;
   private String acao;
   private int custo;
   
   private LinkedList<Integer> adj[]; // Lista de representacao de adjacencia 
   private int V; // Vertices do no
   
   
   
   /**
    * @return the estado
    */
   public String getEstado() {
       return estado;
   }

   /**
    * @param estado the estado to set
    */
   public void setEstado(String estado) {
       this.estado = estado;
   }

   /**
    * @return the pai
    */
   public No getPai() {
       return pai;
   }

   /**
    * @param pai the pai to set
    */
   public void setPai(No pai) {
       this.pai = pai;
   }

   /**
    * @return the acao
    */
   public String getAcao() {
       return acao;
   }

   /**
    * @param acao the acao to set
    */
   public void setAcao(String acao) {
       this.acao = acao;
   }

   /**
    * @return the custo
    */
   public int getCusto() {
       return custo;
   }

   /**
    * @param custo the custo to set
    */
   public void setCusto(int custo) {
       this.custo = custo;
   } 
     
}

