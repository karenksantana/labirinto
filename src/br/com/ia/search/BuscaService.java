package br.com.ia.search;


import java.util.Iterator;
import java.util.Stack;
import java.util.LinkedList;
import java.util.List;
//import java.util.Queue;

import br.com.ia.model.No;

/**
 *
 * @author oberdan.pinheiro
 */
public class BuscaService {

    private Stack<No> arvore = new Stack();
    private List<String> lista = new Stack<String>();
    private No raiz = new No();
    public static final int M = 7, N = 6;
    public static final int[][] MAPA = {
    									{0, 0, 1, 0, 0, 0},
                                        {0, 1, 0, 0, 1, 0},
                                        {0, 0, 0, 0, 0, 1},
                                        {1, 1, 1, 0, 1, 1},
                                        {0, 1, 0, 0, 0, 0},
                                        {0, 1, 0, 1, 1, 0},
                                        {0, 0, 0, 0, 0, 0}
                                        };
    

    public BuscaService(int start[], int goal[]) {    	
    	MAPA[start[0]][start[1]] = 2; // inicio 
    	MAPA[goal[0]][goal[1]] = 3; // fim
    	
    	raiz.setEstado(start[0]+":"+start[1]);
        raiz.setCusto(0);
        arvore.add(raiz);
        lista.add(start[0]+":"+start[1]);
    }
    
   
    
    public String[] solver() {
        String param[] = new String[2];
        int l;
        int c;        
        
        if (!arvore.isEmpty()) {
            No no = arvore.pop();
            param = no.getEstado().split(":");
            l = Integer.parseInt(param[0]);
            c = Integer.parseInt(param[1]);

            if (no.getPai() != null) {
                System.out.print("[" + no.getPai().getEstado() + "]");
            }
            System.out.println("[" + no.getEstado() + "]");

            if (MAPA[l][c] == 3) {
                System.out.println("goal");
                No aux = no.getPai();    
                StringBuilder path = new StringBuilder();
                while (aux != null) {
                    System.out.println("[" + aux.getEstado() + "]");
                    path.append(aux.getEstado());
                    path.append("-");
                    aux = aux.getPai();
                }   
                arvore.clear();
                lista.clear();
                param[0] = "-1";
                param[1] = path.toString();
            } else {

            
                
                // analisa BAIXO (sul)
                if ((l + 1) < M && MAPA[l + 1][c] != 1) {
                    while (!lista.contains((l + 1) + ":" + c)) {
                        No baixo = new No();
                        baixo.setAcao(No.MOVER_BAIXO);
                        baixo.setCusto(no.getCusto() + 1);
                        baixo.setEstado((l + 1) + ":" + c);
                        baixo.setPai(no);
                        arvore.add(baixo);
                        lista.add(baixo.getEstado());
                        continue;
                    }
                }         
                
                // analisa ESQUERDA (oeste)
                if ((c - 1) >= 0 && (c - 1) < N && MAPA[l][c - 1] != 1) {
                    while (!lista.contains(l + ":" + (c - 1))) {
                        No esquerda = new No();
                        esquerda.setAcao(No.MOVER_ESQUERDA);
                        esquerda.setCusto(no.getCusto() + 1);
                        esquerda.setEstado(l + ":" + (c - 1));
                        esquerda.setPai(no);
                        arvore.add(esquerda);
                        lista.add(esquerda.getEstado());
                        continue;
                    }
                }
                
                // analisa DIREITA (leste)
                if ((c + 1) < N && MAPA[l][c + 1] != 1) {
                    while (!lista.contains(l + ":" + (c + 1))) {
                        No direita = new No();
                        direita.setAcao(No.MOVER_DIREITA);
                        direita.setCusto(no.getCusto() + 1);
                        direita.setEstado(l + ":" + (c + 1));
                        direita.setPai(no);
                        arvore.add(direita);
                        lista.add(direita.getEstado());
                        continue;
                    }
                }
                
                // analisa CIMA (norte)
                if ((l - 1) >= 0 && (l - 1) < M && MAPA[l - 1][c] != 1) {
                    if (!lista.contains((l - 1) + ":" + c)) {
                        No cima = new No();
                        cima.setAcao(No.MOVER_CIMA);
                        cima.setCusto(no.getCusto() + 1);
                        cima.setEstado((l - 1) + ":" + c);
                        cima.setPai(no);
                        arvore.add(cima);
                        lista.add(cima.getEstado());
                        
                    
                    }
                }
            }
            
        }
        
        return param;
    }        

}

